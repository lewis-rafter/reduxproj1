import React from 'react';
import logo from './logo.svg';
import './App.css';
import {Provider} from 'react-redux';

import Posts from './components/Posts';
import PostForm from './components/PostForm';

import store from './public/Store';

function App() {
  return (
    <Provider store = {store}>
      <div className="App">
        
        <h1>Welcome to react</h1>

        <hr></hr>

        <PostForm></PostForm>

        <hr></hr>
        <Posts></Posts>
      </div>
    </Provider>
  );
}

export default App;
